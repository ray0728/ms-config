package com.beta.config.security.base;

import org.springframework.security.core.GrantedAuthority;

import java.io.Serializable;

public class Role implements Serializable, GrantedAuthority {
    private String authority;
    public Role(Object role){
        authority = "ROLE_" + (String) role;
    }

    @Override
    public String getAuthority() {
        return authority;
    }
}