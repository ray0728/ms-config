package com.beta.config.controller;

import com.nimbusds.jose.shaded.json.JSONArray;
import com.nimbusds.jose.shaded.json.JSONObject;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class ConfigController {

    @GetMapping("admin")
    public String config(Model model, JwtAuthenticationToken token){
        Jwt credentials = (Jwt)token.getCredentials();
        JSONObject value = (JSONObject) credentials.getClaims().get("realm_access");
        JSONArray roles = (JSONArray) value.get("roles");
        model.addAttribute("title", "configuration");
        model.addAttribute("username", credentials.getClaims().get("preferred_username"));
        model.addAttribute("rights", roles.toJSONString());
        model.addAttribute("accesstoken", credentials.getTokenValue());
        return "config";
    }
}
