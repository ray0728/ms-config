function encrypt(){
    $.ajax({
        url: '/conf/encrypt/',
        type: 'POST',
        contentType: "text/plain",
        data: document.getElementById("plaintext_in").value,
        success: function (data) {
            document.getElementById("ciphertext_out").innerHTML = data;
        },
        error: function (data) {
            document.getElementById("ciphertext_out").innerText = "Error";
        }
    });
};

function decrypt() {
    $.ajax({
        url: '/conf/decrypt/',
        type: 'POST',
        contentType: "text/plain",
        data: document.getElementById("ciphertext_in").value,
        success: function (data) {
            document.getElementById("plaintext_out").innerHTML = data;
        },
        error: function (data) {
            document.getElementById("plaintext_out").innerText = "Error";
        }
    });
}
