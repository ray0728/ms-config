FROM registry.cn-hangzhou.aliyuncs.com/acs/maven:3-jdk-8 AS build-env
LABEL stage=tmp-build

ENV MY_HOME=/app
RUN mkdir -p $MY_HOME
WORKDIR $MY_HOME
ADD pom.xml $MY_HOME

RUN ["/usr/local/bin/mvn-entrypoint.sh", "mvn", "validate", "clean", "--fail-never"]
ADD . $MY_HOME

RUN ["/usr/local/bin/mvn-entrypoint.sh", "mvn", "verify", "-Dmaven.test.skip=true"]

FROM openjdk:8-jre-alpine
COPY --from=build-env /app/target/*.jar /app.jar
ADD UnlimitedJCEPolicyJDK8/*.jar /usr/lib/jvm/default-jvm/jre/lib/security/policy/unlimited/
ADD run.sh run.sh
RUN chmod a+x run.sh
