#!/bin/sh
java $JAVA_OPTIONS \
     -Dserver.port=$PORT \
     -Dkeycloak.hostname=$SERVERHOST \
     -Dkeycloak.realm=$REALM \
     -Dspring.security.role.admin=$ROLE_ADMIN \
     -Dspring.cloud.config.server.git.uri=$GIT_SERVER_URI \
     -Dspring.cloud.config.server.git.username=$GIT_USERNAME \
     -Dspring.cloud.config.server.git.password=$GIT_PASSWORD \
     -Dspring.cloud.config.server.git.search-paths=$GIT_SEARCH_PATH \
     -Ddiscovery.server=$DISCOVERY_HOST \
     -Dhost.ipaddress=$HOST_IP \
     -Dtrace=$DEBUG \
     -jar /app.jar 

